import java.util.Scanner;

public class App 
{
	public static void main(String[] args) 
	{
		System.out.println("Adios");
		findFactorial();
		conversion();
	}
	public static void findFactorial() 
	{
		System.out.println("Please enter a number to find its factorial");
		Scanner input = new Scanner(System.in);
		int num = input.nextInt();
		int i = 1;
		int f = 1;
		do 
		{
			f = f * i;
			i++;
		}
		while(i <= num);
		System.out.println("Factorial of " +num+ " is "+f);
		System.out.println("***************");
		
	}
	public static void conversion() 
	{
		System.out.println("Please enter an integer to convert it to float");
		Scanner input = new Scanner(System.in);
		int num = input.nextInt();
		float f = (float)num;
		System.out.println(num+ " is converted to "+f);
		System.out.println("***************");
		
		System.out.println("Please enter an double to convert it to integer");
		double d = input.nextDouble();
		int num1 = (int)d;
		System.out.println(d+ " is converted to "+num1);
		System.out.println("***************");
	}
}
